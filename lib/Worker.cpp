#include "./Worker.hpp"

Worker::Worker(int limit):
  limit(limit)
{
}

Worker::~Worker() {}

void Worker::work(coroutine<void>::push_type &sink) {
  for(int i = 0; i < this->limit; i++) {
    std::cout << '\t' <<"Work " << i << std::endl;
    sink();
  }
}

void Worker::run() {
  using std::placeholders::_1;
  coroutine<void>::pull_type source{std::bind(&Worker::work, this, _1)};
  while (source) {
    source();
  }
}

