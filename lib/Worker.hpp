#ifndef __boilerplate_lib_Worker__
#define __boilerplate_lib_Worker__

#include <functional>
#include <iostream>
#include <string>

#define BOOST_COROUTINES_NO_DEPRECATION_WARNING
#include <boost/coroutine/all.hpp>

using namespace boost::coroutines;

class Worker {
  public:
    Worker(int);
    ~Worker();
    void run();
  protected:
    int limit;
    void work(coroutine<void>::push_type &sink);
};

#endif // __boilerplate_lib_Worker__

