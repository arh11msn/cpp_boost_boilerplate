#include <iostream>

#include "./BoilerplateApp.hpp"

BoilerplateApp::BoilerplateApp():
  name(std::string("BoilerplateApp")),
  worker(Worker(10))
{}

BoilerplateApp::~BoilerplateApp() {}

void BoilerplateApp::run() {
  std::cout << "Running app \'" << this->name << "\'" << std::endl;
  this->worker.run();
}

