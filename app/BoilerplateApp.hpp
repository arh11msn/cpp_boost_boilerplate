#ifndef __boilerplate_app_BoilerplateApp__
#define __boilerplate_app_BoilerplateApp__

#include <string>

#include "../lib/Worker.hpp"

class BoilerplateApp {
  public:
    BoilerplateApp();
    ~BoilerplateApp();
    void run();
  protected:
    std::string name;
    Worker worker;
};

#endif // __boilerplate_app_BoilerplateApp__

