#include "./app/BoilerplateApp.hpp"

int main(int argc, char** argv) {
  auto app = BoilerplateApp();
  app.run();
  return 0;
}

